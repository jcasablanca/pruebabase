package org.laberit.repository;

import java.util.List;

import org.laberit.daos.EmpleadoForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmpleadoRepository extends JpaRepository<EmpleadoForm, Long>{
	List<EmpleadoForm> findByNombre(String nombre);
	List<EmpleadoForm> findByNombreContains(String nombre);
}

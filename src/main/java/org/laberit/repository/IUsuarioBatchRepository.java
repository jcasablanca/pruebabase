package org.laberit.repository;

import org.laberit.daos.UsuarioBatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioBatchRepository extends JpaRepository<UsuarioBatch, Long> {
	
	UsuarioBatch findByUsername(String username);
	
}

package org.laberit.repository;

import org.laberit.jdbc.Empleado;

public interface IEmpleadoRepositoryJDBC {
	void insertar (Empleado empleado);
}

package org.laberit.repository;

import org.laberit.jdbc.Empleado;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EmpleadoRepositoryJDBCImpl implements IEmpleadoRepositoryJDBC {

	private JdbcTemplate plantilla;

	public EmpleadoRepositoryJDBCImpl(JdbcTemplate plantilla) {
		super();
		this.plantilla = plantilla;
	}

	@Override
	public void insertar(Empleado empleado) {
		plantilla.update("insert into Empleado_FORM values (?,?,?)", empleado.getId(), empleado.getNombre(),
				empleado.getEmail());
	}

}

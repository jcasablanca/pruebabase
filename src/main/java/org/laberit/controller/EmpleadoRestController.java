package org.laberit.controller;

import java.util.List;

import org.laberit.daos.EmpleadoForm;
import org.laberit.service.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EmpleadoRestController {
	
	@Autowired
	IEmpleadoService empleadoService;
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	//@PreAuthorize("hasRole('ROLE_ADMIN') AND hasRole('ROLE_USER_ADMIN')")
	@Secured({"ROLE_ADMIN","ROLE_USER_ADMIN"})
	@GetMapping("empleados")
	public List<EmpleadoForm> listar(){
		return empleadoService.listar();
	}
	
	//@PreAuthorize("hasRole('ROLE_USER')")
	@Secured("ROLE_USER")
	@GetMapping("empleado/{idEmpleado}")
	public EmpleadoForm obtenerEmpleado(@PathVariable String idEmpleado){
		return empleadoService.getEmpleado(Long.valueOf(idEmpleado));
	}
	
	@PostMapping("empleado")
	public EmpleadoForm crearEmpleado(@RequestBody EmpleadoForm empleado) {
		try {
			return empleadoService.addEmpleado(empleado);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PutMapping("empleado")
	public EmpleadoForm modificarEmpleado(@RequestBody EmpleadoForm empleado) {
		try {
			return empleadoService.addEmpleado(empleado);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@DeleteMapping("empleado/{idEmpleado}")
	public void eliminarEmpleado(@PathVariable String idEmpleado) {
		empleadoService.borrarEmpleado(Long.valueOf(idEmpleado));
	}
}

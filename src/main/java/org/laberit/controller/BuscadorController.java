package org.laberit.controller;

import java.util.List;

import org.laberit.daos.Buscador;
import org.laberit.daos.EmpleadoForm;
import org.laberit.service.IBuscadorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BuscadorController {
	
	Logger log = LoggerFactory.getLogger(BuscadorController.class);

	@Autowired
	IBuscadorService service;
	
	@GetMapping("/buscador")
	public String buscador(Model model) {
		
		model.addAttribute("buscador", new Buscador());
		return "buscador";
	}
	
	@PostMapping("/buscador")
	public String buscar(Buscador buscador, Model model) {
		log.info("Se lanza la búsqueda de empleados");
		log.debug("Los parámetros de búsqueda utilizados son: "+buscador.getNombre());
		List<EmpleadoForm> lista = service.obtenerListaDeEmpleados(buscador);
		model.addAttribute("empleados", lista);
		log.debug("El conjunto total de resultados a mostrar es: "+lista.size());
		return "buscador";
	}
	
}

package org.laberit.controller;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.laberit.daos.EmpleadoForm;
import org.laberit.service.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EmpleadoController {
	
	@Autowired
	IEmpleadoService empleadoService;
	
	@PostMapping("addEmpleado")
	public String addEmpleado(@Valid EmpleadoForm empleado,BindingResult bindingResult, Model model) {
		if(bindingResult.hasErrors()) {
			return "empleado";
		}
		try {
			model.addAttribute("empleados", empleadoService.addEmpleado(empleado));
		} catch (Exception e) {
			model.addAttribute("error", "Se ha producido un error en la carga de los datos contra la BBDD");
			return "empleado";
		}
		return "addEmpleado";
	}
	
	@GetMapping("empleado")
	public String verEmpleado(Model model) {
		model.addAttribute("empleadoForm", new EmpleadoForm());
		return "empleado";
	}
	
}

package org.laberit.batch;

import org.laberit.daos.Usuario;
import org.laberit.daos.UsuarioBatch;
import org.springframework.batch.item.ItemProcessor;

import net.bytebuddy.utility.RandomString;

public class UsuarioItemProcessor implements ItemProcessor<Usuario, UsuarioBatch> {

	@Override
	public UsuarioBatch process(Usuario usuario) throws Exception {
		UsuarioBatch usuarioBatch = new UsuarioBatch();
		usuarioBatch.setUsername(usuario.getUsername()+RandomString.make(8));
		usuarioBatch.setPassword(usuario.getPassword());
		usuarioBatch.setRol(usuario.getRol());
		return usuarioBatch;
	}

}

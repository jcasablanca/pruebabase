package org.laberit.batch;

import java.util.Iterator;

import org.laberit.daos.Usuario;
import org.laberit.repository.IUsuarioRepository;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

public class UsuarioItemReader implements ItemReader<Usuario> {

	@Autowired
	private IUsuarioRepository repo;
	
	private Iterator<Usuario> iterator;
	
	@BeforeStep
	public void listar() {
		iterator = repo.findAll().iterator();
	}
	
	@Override
	public Usuario read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if(iterator!=null &&iterator.hasNext()) {
			return iterator.next();
		}else {
			return null;
		}
	}

}

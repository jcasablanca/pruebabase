package org.laberit.batch;

import java.util.List;

import org.laberit.daos.UsuarioBatch;
import org.laberit.repository.IUsuarioBatchRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

public class UsuarioItemWriter implements ItemWriter<UsuarioBatch> {

	@Autowired
	private IUsuarioBatchRepository repoBatch;
	
	@Override
	public void write(List<? extends UsuarioBatch> items) throws Exception {
		repoBatch.saveAll(items);
	}

}

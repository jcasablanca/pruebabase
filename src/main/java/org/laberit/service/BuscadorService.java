package org.laberit.service;

import java.util.List;

import org.laberit.daos.Buscador;
import org.laberit.daos.EmpleadoForm;
import org.laberit.repository.IEmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuscadorService implements IBuscadorService {
	
	Logger log = LoggerFactory.getLogger(BuscadorService.class);

	@Autowired
	IEmpleadoRepository repositorio;
	
	@Override
	public List<EmpleadoForm> obtenerListaDeEmpleados(Buscador buscador) {
		log.info("Se realiza la búsqueda de información a través de la BBDD.");
		return repositorio.findByNombreContains(buscador.getNombre());
	}
	
}

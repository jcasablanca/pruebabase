package org.laberit.service;

import java.util.List;

import org.laberit.daos.Buscador;
import org.laberit.daos.EmpleadoForm;

public interface IBuscadorService {

	List<EmpleadoForm> obtenerListaDeEmpleados(Buscador buscador);
	
}

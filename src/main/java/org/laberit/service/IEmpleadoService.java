package org.laberit.service;

import java.util.List;

import org.laberit.daos.EmpleadoForm;

public interface IEmpleadoService {

	EmpleadoForm addEmpleado(EmpleadoForm empleado) throws Exception;
	
	List<EmpleadoForm> listar();
	
	EmpleadoForm getEmpleado(Long id);
	
	void borrarEmpleado(Long id);
	
}

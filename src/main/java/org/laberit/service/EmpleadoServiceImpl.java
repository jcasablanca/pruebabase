package org.laberit.service;

import java.util.ArrayList;
import java.util.List;

import org.laberit.daos.EmpleadoForm;
import org.laberit.jdbc.Empleado;
import org.laberit.repository.EmpleadoRepositoryJDBCImpl;
import org.laberit.repository.IEmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {
	
	Logger log = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

	List<EmpleadoForm> lista = new ArrayList<EmpleadoForm>();
	
	@Autowired
	IEmpleadoRepository repositorio ;
	
	@Autowired
	EmpleadoRepositoryJDBCImpl repositorioJDBC;
	
	@Transactional
	@Override
	public EmpleadoForm addEmpleado(EmpleadoForm empleadoForm) throws Exception {
		try {
			log.info("Iniciamos la creación de un usuario");
//			Empleado empleado = new Empleado();
//			empleado.setEmail(empleadoForm.getEmail());
//			empleado.setNombre(empleadoForm.getNombre());
			log.debug("Se almacena el usuario en BBDD");
			repositorio.save(empleadoForm);
			return empleadoForm;
		}catch (Exception e) {
			log.error("Se ha producido una excepcion: "+e.getMessage());
			throw new Exception();
		}
	}

	@Override
	public List<EmpleadoForm> listar(){
		return repositorio.findAll();
	}

	@Override
	public EmpleadoForm getEmpleado(Long id) {
		return repositorio.findById(id).get();
	}

	@Override
	public void borrarEmpleado(Long id) {
		repositorio.deleteById(id);
	}

}

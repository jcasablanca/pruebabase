package org.laberit;

import java.util.List;

import javax.annotation.PostConstruct;

import org.laberit.service.LdapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaBaseApplication {
	
	@Autowired
	private LdapService ldapService;

	public static void main(String[] args) {
		SpringApplication.run(PruebaBaseApplication.class, args);
	}
	
	@PostConstruct
	public void cargaUsuarios() {
		List<String> lista = ldapService.getAllUsers();
		System.out.println(lista);
	}

}

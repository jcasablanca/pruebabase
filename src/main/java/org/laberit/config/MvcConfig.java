package org.laberit.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

	public void addViewControllers(ViewControllerRegistry registry) {
//		registry.addViewController("/buscador").setViewName("buscador");
//		registry.addViewController("/").setViewName("buscador");
//		registry.addViewController("/empleado").setViewName("empleado");
		registry.addViewController("/login").setViewName("login");
//		registry.addViewController("/h2-console");
	}

}

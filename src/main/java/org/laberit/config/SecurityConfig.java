package org.laberit.config;

import org.laberit.security.JWTAuthenticationFilter;
import org.laberit.security.JWTAuthorizationFilter;
import org.laberit.service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true) // @Secured
//@EnableGlobalMethodSecurity(prePostEnabled = true) //@PreAuthorized
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(usuarioService);
//		logger.info("Definición de la conexión con el LDAP");
//		auth
//			.ldapAuthentication()
//			.userSearchFilter("(uid={0})")
//			.contextSource()
//			.url("ldap://localhost:8689/dc=laberit,dc=com");
			
	}
	
	@Bean
	public JWTAuthorizationFilter jwtAuthorizationFilterBean() {
		return new JWTAuthorizationFilter();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.cors().disable()
			.csrf().disable()
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
				.antMatchers("/buscador", "/h2-console","/auth","/addUsuarios").permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.permitAll();
		http.headers().frameOptions().disable();
		
		//Configuración para JWT
//		http
//		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//		.cors().and()
//		.csrf().disable()
//		//.authorizeRequests().antMatchers(HttpMethod.POST, LOGIN_URL).permitAll()
//		.authorizeRequests()
//			.antMatchers("/login").permitAll()
//			.anyRequest().authenticated()
//			.and()
//			.addFilterBefore(new JWTAuthenticationFilter(authenticationManager()),
//					UsernamePasswordAuthenticationFilter.class)
//			.addFilterBefore(jwtAuthorizationFilterBean(), UsernamePasswordAuthenticationFilter.class);
//			.addFilter(new JWTAuthenticationFilter(authenticationManager()))
//			.addFilter(new JWTAuthorizationFilter(authenticationManager()));
	}
} 


package org.laberit.config;

import org.laberit.batch.UsuarioItemProcessor;
import org.laberit.batch.UsuarioItemReader;
import org.laberit.batch.UsuarioItemWriter;
import org.laberit.daos.Usuario;
import org.laberit.daos.UsuarioBatch;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class JobBatchConfig {

	@Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    
    @Autowired
	Job job;
	
	@Autowired
	JobLauncher jobLauncher;
	
	@Bean
	public UsuarioItemReader reader() {
		return new UsuarioItemReader();
	}
	
	@Bean
	public UsuarioItemProcessor processor() {
		return new UsuarioItemProcessor();
	}
	
	@Bean
	public UsuarioItemWriter writer() {
		return new UsuarioItemWriter();
	}
	
	@Bean
	public Job job(Step step) {
		return jobBuilderFactory.get("job1")
											.flow(step)
											.end()
											.build();
	}
	
	@Bean
	public Step step(UsuarioItemReader reader, UsuarioItemWriter writer, UsuarioItemProcessor processor) {
		return stepBuilderFactory.get("step").allowStartIfComplete(true)
			.<Usuario, UsuarioBatch>chunk(10)
			.reader(reader)
			.processor(processor)
			.writer(writer)
			.build();
	}
	
	@Scheduled(cron="${spring.batch.cron}")
	public void ejecutarCron() throws Exception{
		jobLauncher.run(job, new JobParameters());
	}
}

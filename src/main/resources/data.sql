INSERT INTO empleado_form (id, nombre, email) VALUES (1, 'Elena Casado Ramos', 'elenita@gmail.com');
INSERT INTO empleado_form (id, nombre, email) VALUES (2, 'Juan López Riduejo', 'aquiestamos@gmail.com');
INSERT INTO empleado_form (id, nombre, email) VALUES (3, 'Marco Giovanni Grissini', 'pasabaporaqui@gmail.com');

INSERT INTO usuario (id, username, password, rol) VALUES (1, 'user', '$2a$10$xKCRQBq.3HxHJZTPpxi9.Oh/CdRHk8XpVRCZZqcMO0Z1MvVHnOHoK', 'USER');
INSERT INTO usuario (id, username, password, rol) VALUES (2, 'admin', '$2a$10$xKCRQBq.3HxHJZTPpxi9.Oh/CdRHk8XpVRCZZqcMO0Z1MvVHnOHoK', 'ADMIN');
INSERT INTO usuario (id, username, password, rol) VALUES (3, 'user_admin', '$2a$10$xKCRQBq.3HxHJZTPpxi9.Oh/CdRHk8XpVRCZZqcMO0Z1MvVHnOHoK', 'USER_ADMIN');